package mobi.wiegandtech.mysticalmiriam;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

public class MainWindow extends Activity {
	protected final static int DIALOG_RESULTS = 1234576;
	protected final static int TIME_TO_THINK = 4000; // 4 seconds
	private int mCountFortunes = 0;

	private AlphaAnimation mAnim = null;
	private AnimationDrawable mAnimThumb = null;
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			removeDialog(DIALOG_RESULTS);
			showDialog(DIALOG_RESULTS);
		}
	};
	private Thread mThread = null;
	private Random mRandom = new Random();
	private String mLastFortune = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		ImageView thumb = (ImageView) findViewById(R.id.ImageView02);
		if (thumb != null) {
			thumb.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						startAnimations(v);
						return true;
					}
					return false;
				}
			});
			thumb.setBackgroundResource(R.drawable.thumbscan);
			mAnimThumb = (AnimationDrawable) thumb.getBackground();
		}
	}

	protected void startAnimations(View v) {
		mThread = new Thread() {
			@Override
			public void run() {
				// wait 2 seconds
				try {
					Thread.sleep(TIME_TO_THINK + 300);
				} catch (InterruptedException e) {
				}
				if (mHandler != null)
					mHandler.sendEmptyMessage(0);
			}
		};
		mThread.start();

		View imgHeader = findViewById(R.id.ImageView01);
		if (imgHeader != null) {
			mAnim = new AlphaAnimation(1, 0);
			mAnim.setRepeatMode(AlphaAnimation.REVERSE);
			mAnim.setDuration(TIME_TO_THINK / 6);
			mAnim.setRepeatCount(6);
			imgHeader.setAnimation(mAnim);
			imgHeader.startAnimation(mAnim);
		}

		mAnimThumb.start();
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_RESULTS:
			generateFortune();
			mAnimThumb.stop();
			mAnimThumb.selectDrawable(0); // reset to first frame
			
			AlertDialog.Builder adbuilder = new AlertDialog.Builder(this);
			adbuilder.setTitle("Mystical Miriam says...");
			adbuilder.setMessage(mLastFortune);
			adbuilder.setPositiveButton("Thank you!", null);
			adbuilder.setCancelable(true);
			return adbuilder.create();
		}
		return null;
	}

	private static final String[] part1 = { "2", "2", "3", "3", "4", "4", "5",
			"6", "7", "8", "9" };
	private static final String[] part2 = { "children", "kittens",
			"lampshades", "jobs", "careers", "homes", "cities", "bands",
			"puppies", "spouses", "cars" };
	private static final String[] part3 = { "purple", "green", "red", "blue",
			"brown", "white", "transparent", "black", "grey", "gray", "yellow",
			"orange" };
	private static final String[] part4 = { "laptop", "phone", "hat", "shirt",
			"cup", "chalice", "ring", "bridge", "bride", "book", "soda pop",
			"flag" };
	private static final String[] part5 = { "slow walking", "running",
			"jumping", "sick", "glowing", "drooling", "dancing", "bicycling",
			"roller blading", "flying" };
	private static final String[] part6 = { "man", "woman", "student",
			"professor", "librarian", "Commodore", "robot", "teacher", "pilot" };
	private static final String[] part7 = { "run away quickly",
			"say 'Thank you'", "walk carefully AROUND", "beg for your life" };
	private static final String[] part8 = { "your just reward",
			"a pie in the face", "a bird in the hand",
			"a hamburger free of charge", "a raise",
			"a call from the President", "a $100 bill",
			"a ticket for loitering" };

	private String getRandomString(String[] array) {
		int i = mRandom.nextInt(array.length);
		return array[i];
	}

	public String generateFortune() {
		mRandom.setSeed(System.currentTimeMillis());
		StringBuilder ret = new StringBuilder();
		mCountFortunes++;
		if (mCountFortunes == 4) {
			ret.append("I am too fatigued, ");
			ret.append("come see me later after I get some rest.");
		} else if (mCountFortunes == 5) {
			ret.append("Since you will not let me rest, I curse you!");
			ret.append(" Tonight you shall get no rest, for ");
			ret.append(getRandomString(part8));
			ret.append(" shall come and you shall turn and toss!");
		} else if (mCountFortunes == 8) {
			ret.append("Hi! You've reached Mystical Miriam's Crystal Ball!");
			ret.append(" I'm not available right now, but leave your name, ");
			ret.append(" age and shoe size");
			ret.append(" and I will return your call later. Thank you.");
		} else {
			ret.append("I see ");
			ret.append(getRandomString(part1));
			ret.append(" ");
			ret.append(getRandomString(part2));
			ret.append(" in your future. Beware of the ");
			ret.append(getRandomString(part3));
			ret.append(" ");
			ret.append(getRandomString(part4));
			ret.append(", for it is a terrible omen!");
			ret.append(" Finally, if you should meet the ");
			ret.append(getRandomString(part5));
			ret.append(" ");
			ret.append(getRandomString(part6));
			ret.append(", ");
			ret.append(getRandomString(part7));
			ret.append(", for you may soon receive ");
			ret.append(getRandomString(part8));
			ret.append(".");
		}
		mLastFortune = ret.toString();
		return mLastFortune;
	}
}